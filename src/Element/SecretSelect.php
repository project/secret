<?php declare(strict_types = 1);

namespace Drupal\secret\Element;

use Drupal\Core\Render\Element\Select;
use Drupal\secret\Contract\ElementProcessor\SelectProcessor;
use Drupal\secret\SecretStorageInterface;

/**
 * Provides a select form element that displays available keys.
 *
 * Properties:
 * - #secret_path:
 *
 * @FormElement("secret_select")
 */
final class SecretSelect extends Select {

  /**
   * {@inheritdoc}
   */
  public function getInfo(): array {
    $info = parent::getInfo();
    $info['#secret_path'] = 'secrets';
    \array_unshift($info['#process'], [self::class, 'processSecretSelect']);
    return $info;
  }

  /**
   * Processes a secret select form element.
   */
  public static function processSecretSelect(array &$element): array {
    if (!\is_string($element['#secret_path'])) {
      throw new \InvalidArgumentException('#secret_path must be a string.');
    }
    if ($element['#secret_path'] === '') {
      throw new \InvalidArgumentException('#secret_path must not be empty.');
    }

    if (!isset($element['#description'])) {
      $storage = self::getSecretStorage();
      if ($storage instanceof SelectProcessor) {
        $element = $storage->processSelect($element);
      }
    }

    $options = self::getSecretStorage()->list($element['#secret_path']);
    foreach ($options as $key => $value) {
      $element['#options'][$element['#secret_path'] . '/' . $key] = $key;
    }

    if (!isset($element['#description'])) {
      $storage = self::getSecretStorage();
      if ($storage instanceof SelectProcessor) {
        $element = $storage->processSelect($element);
      }
    }

    return $element;
  }

  /**
   * {@selfdoc}
   */
  private static function getSecretStorage(): SecretStorageInterface {
    return \Drupal::service('secret.storage');
  }

}
