<?php declare(strict_types = 1);

namespace Drupal\secret\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Textfield;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\secret\Contract\ElementProcessor\TextfieldProcessor;
use Drupal\secret\SecretStorageInterface;

/**
 * Provides a select form element that displays available keys.
 *
 * Properties:
 * - #secret_path:
 *
 * @FormElement("secret_textfield")
 */
final class SecretTextfield extends Textfield {

  /**
   * {@inheritdoc}
   */
  public function getInfo(): array {
    $info = parent::getInfo();
    $info['#secret_validate'] = TRUE;
    $info['#process'][] = [self::class, 'processSecretElement'];
    return $info;
  }

  /**
   * Processes a secret select form element.
   */
  public static function processSecretElement(array &$element): array {
    if ($element['#secret_validate']) {
      $element['#element_validate'][] = [self::class, 'validateSecretElement'];
    }

    $storage = self::getSecretStorage();
    if ($storage instanceof TextfieldProcessor) {
      $element = $storage->processTextfield($element);
    }
    return $element;
  }

  /**
   * Validation callback.
   */
  public static function validateSecretElement(array &$element, FormStateInterface $form_state): void {
    if (!self::getSecretStorage()->has($element['#value'])) {
      $message = new TM('The secret path @path does not exist.', ['@path' => $element['#value']]);
      $form_state->setError($element, $message);
    }
  }

  /**
   * {@selfdoc}
   */
  private static function getSecretStorage(): SecretStorageInterface {
    return \Drupal::service('secret.storage');
  }

}
