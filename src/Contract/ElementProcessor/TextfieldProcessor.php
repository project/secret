<?php declare(strict_types = 1);

namespace Drupal\secret\Contract\ElementProcessor;

/**
 * Defines a processor for 'secret_textfield' form element.
 */
interface TextfieldProcessor {

  /**
   * Returns a processed element.
   */
  public function processTextfield(array $element): array;

}
