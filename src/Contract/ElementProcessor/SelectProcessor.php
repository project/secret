<?php declare(strict_types = 1);

namespace Drupal\secret\Contract\ElementProcessor;

/**
 * Defines a processor for 'secret_select' form element.
 */
interface SelectProcessor {

  /**
   * Returns a processed element.
   */
  public function processSelect(array $element): array;

}
