<?php declare(strict_types = 1);

namespace Drupal\secret;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Site\Settings as SiteSettings;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;
use Drupal\secret\Contract\ElementProcessor\SelectProcessor;
use Drupal\secret\Contract\ElementProcessor\TextfieldProcessor;

/**
 * Implements storage for site secrets using Drupal settings.
 */
final class Storage implements SecretStorageInterface, TextfieldProcessor, SelectProcessor {

  /**
   * {@inheritdoc}
   */
  public function get(string $path): string|int|float|null {
    $all_settings = self::getSiteSettings()::getAll();
    $path_items = \explode('/', $path);
    $value = NestedArray::getValue($all_settings, $path_items);
    // The secrets are manually configured by the users. Therefore, in the case
    // of incorrect configuration, no exception is thrown.
    if (!self::isValidSecret($value)) {
      return NULL;
    }
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function list(string $directory_path): array {
    $all_settings = self::getSiteSettings()::getAll();
    $path_items = \explode('/', $directory_path);
    $list = NestedArray::getValue($all_settings, $path_items);
    return \is_array($list) ?
      \array_filter($list, [self::class, 'isValidSecret']) : [];
  }

  /**
   * {@inheritdoc}
   */
  public function has(string $path): bool {
    $all_settings = self::getSiteSettings()::getAll();
    $path_items = \explode('/', $path);
    $key_exists = FALSE;
    $value = NestedArray::getValue($all_settings, $path_items, $key_exists);
    return $key_exists && self::isValidSecret($value);
  }

  /**
   * {@inheritdoc}
   */
  public function processTextfield(array $element): array {
    if (!isset($element['#description'])) {
      $element['#description'] = new TM(
        "A slash separated string that defines a path in nested settings array.<br/>For example secrets/foo/bar path points to \$settings['secrets']['foo']['bar'] variable.",
      );
    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function processSelect(array $element): array {
    if (!isset($element['#description'])) {
      $path_items = \explode('/', $element['#secret_path']);
      $variable = "\$settings['" . \implode("']['", $path_items) . "']";
      $element['#description'] = new TM(
        'The available options must be configured in %variable variable in your local settings.php file.',
        ['%variable' => $variable],
      );
    }
    return $element;
  }

  /**
   * {@selfdoc}
   */
  private static function isValidSecret(mixed $secret): bool {
    return \is_null($secret) || \is_scalar($secret);
  }

  /**
   * {@selfdoc}
   */
  private static function getSiteSettings(): SiteSettings {
    return \Drupal::service('settings');
  }

}
