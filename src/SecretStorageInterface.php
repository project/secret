<?php declare(strict_types = 1);

namespace Drupal\secret;

/**
 * Interface for secret storages.
 */
interface SecretStorageInterface {

  /**
   * Returns secret value or null if the secret does not exist.
   *
   * Note that the secret value must be a scalar or null.
   */
  public function get(string $path): string|int|float|null;

  /**
   * Returns secret labels keyed by secret path for a given directory.
   */
  public function list(string $directory_path): array;

  /**
   * Returns true if the storage can return a value for the given secret path.
   */
  public function has(string $path): bool;

}
