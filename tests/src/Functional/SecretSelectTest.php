<?php declare(strict_types = 1);

namespace Drupal\Tests\secret\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests 'secret_select' form element.
 *
 * @group secret
 */
final class SecretSelectTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['secret', 'secret_test'];

  /**
   * {@selfdoc}
   */
  public function testElement(): void {
    $admin_user = $this->drupalCreateUser();
    $this->drupalLogin($admin_user);
    $this->drupalGet('/admin/test/secret-select');

    // -- Required.
    $expected = [
      '' => '- Select -',
      'secrets/alpha' => 'alpha',
      'secrets/beta' => 'beta',
      'secrets/gamma' => 'gamma',
    ];
    $this->assertSecrets('secret_1', $expected, "\$settings['secrets']");

    // -- Not required.
    $expected = [
      'secrets/alpha' => 'alpha',
      'secrets/beta' => 'beta',
      'secrets/gamma' => 'gamma',
    ];
    $this->assertSecrets('secret_2', $expected, "\$settings['secrets']");

    // -- Custom path.
    $expected = [
      'secrets/nested/foo' => 'foo',
      'secrets/nested/bar' => 'bar',
    ];
    $this->assertSecrets('secret_3', $expected, "\$settings['secrets']['nested']");

    // -- Path without children.
    $this->assertSecrets('secret_4', [], "\$settings['secrets']['alpha']");

    // -- Wrong path.
    $this->assertSecrets('secret_5', [], "\$settings['not']['existing']['path']");

    // -- Test submission.
    $edit = [
      'secret_1' => 'secrets/beta',
      'secret_3' => 'secrets/nested/bar',
    ];
    $this->submitForm($edit, 'Submit');

    $values = $this->getSubmissionValues();
    self::assertSame('secrets/beta', $values->secret_1);
    self::assertSame('secrets/alpha', $values->secret_2);
    self::assertSame('secrets/nested/bar', $values->secret_3);
    self::assertSame('', $values->secret_4);
    self::assertSame('', $values->secret_5);
  }

  /**
   * {@selfdoc}
   */
  private function assertSecrets(string $name, array $expected_secrets, string $variable): void {
    // Make sure that select element exists.
    $xpath = \sprintf('//select[@name = "%s"]', $name);
    $select = $this->getSession()->getPage()->find('xpath', $xpath);
    $options = $select->findAll('xpath', '/option');
    $actual_secrets = self::buildSecrets($options);
    self::assertSame($expected_secrets, $actual_secrets);

    $expected_description = \sprintf(
      'The available options must be configured in %s variable in your local settings.php file.',
      $variable,
    );
    $actual_description = $this->getSession()
      ->getPage()
      ->find('xpath', $xpath . '/following-sibling::div[@class = "form-item__description"]')
      ->getText();
    self::assertSame($expected_description, $actual_description);

  }

  /**
   * {@selfdoc}
   */
  private static function buildSecrets(array $options): array {
    $secrets = [];
    foreach ($options as $option) {
      $secrets[$option->getValue()] = $option->getHtml();
    }
    return $secrets;
  }

  /**
   * {@selfdoc}
   */
  private function getSubmissionValues(): \stdClass {
    $values_encoded = $this->getSession()
      ->getPage()
      ->find('css', '.messages__content')
      ->getText();
    return \json_decode($values_encoded);
  }

}
