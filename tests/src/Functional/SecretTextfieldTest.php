<?php declare(strict_types = 1);

namespace Drupal\Tests\secret\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests 'secret' form element.
 *
 * @group secret
 */
final class SecretTextfieldTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['secret', 'secret_test'];

  /**
   * Test callback.
   */
  public function testElement(): void {
    $admin_user = $this->drupalCreateUser();
    $this->drupalLogin($admin_user);
    $this->drupalGet('/admin/test/secret');

    $description_1 = 'A slash separated string that defines a path in nested settings array.';
    $description_2 = "For example secrets/foo/bar path points to \$settings['secrets']['foo']['bar'] variable.";

    $xpath = <<< XPATH
      //label[text() = 'Secret 1']
      /following-sibling::input[@name = "secret_1" and @value = "secrets/alpha"]
      /following-sibling::div
      [@class = "form-item__description" and contains(., "$description_1") and contains(., "$description_2")]
      XPATH;
    $this->assertSession()->elementExists('xpath', $xpath);

    $xpath = <<< XPATH
      //label[text() = 'Secret 2']
      /following-sibling::input[@name = "secret_2" and @value = "not/existing/path/beta"]
      /following-sibling::div
      [@class = "form-item__description" and contains(., "$description_1") and contains(., "$description_2")]
      XPATH;
    $this->assertSession()->elementExists('xpath', $xpath);

    $xpath = <<< XPATH
      //label[text() = 'Secret 3']
      /following-sibling::input[@name = "secret_3" and @value = "not/existing/path/gamma"]
      /following-sibling::div
      [@class = "form-item__description" and contains(., "$description_1") and contains(., "$description_2")]
      XPATH;
    $this->assertSession()->elementExists('xpath', $xpath);

    // -- Test submission.
    $this->submitForm([], 'Submit');

    $this->assertSession()->statusMessageContains('The secret path not/existing/path/beta does not exist.', 'error');
    $this->assertSession()->statusMessageNotContains('The secret path not/existing/path/gamma does not exist.', 'error');

    $this->assertSession()->elementExists('xpath', '//input[@name = "secret_2" and @aria-invalid]');
    $this->assertSession()->elementNotExists('xpath', '//input[@name = "secret_3" and @aria-invalid]');

    $edit = ['secret_2' => 'secrets/beta'];
    $this->submitForm($edit, 'Submit');
    $values = $this->getSubmissionValues();
    self::assertSame('secrets/alpha', $values->secret_1);
    self::assertSame('secrets/beta', $values->secret_2);
    self::assertSame('not/existing/path/gamma', $values->secret_3);
  }

  /**
   * {@selfdoc}
   */
  private function getSubmissionValues(): \stdClass {
    $values_encoded = $this->getSession()
      ->getPage()
      ->find('css', '.messages__content')
      ->getText();
    return \json_decode($values_encoded);
  }

}
