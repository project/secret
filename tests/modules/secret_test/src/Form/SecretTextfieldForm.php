<?php declare(strict_types = 1);

namespace Drupal\secret_test\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;

/**
 * Provides a form for testing 'secret' element.
 */
final class SecretTextfieldForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'secret_test_test';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $test_settings['secrets'] = [
      'alpha' => 'secret_value',
      'beta' => 'secret_value',
      'gamma' => 'secret_value',
    ];
    new Settings(Settings::getAll() + $test_settings);

    $form['secret_1'] = [
      '#type' => 'secret_textfield',
      '#title' => new TM('Secret 1'),
      '#required' => TRUE,
      '#default_value' => 'secrets/alpha',
    ];

    $form['secret_2'] = [
      '#type' => 'secret_textfield',
      '#title' => new TM('Secret 2'),
      '#default_value' => 'not/existing/path/beta',
      '#secret_validate' => TRUE,
    ];

    $form['secret_3'] = [
      '#type' => 'secret_textfield',
      '#title' => new TM('Secret 3'),
      '#default_value' => 'not/existing/path/gamma',
      '#secret_validate' => FALSE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => new TM('Submit'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();
    $this->messenger()->addStatus(\json_encode($values));
  }

}
