<?php declare(strict_types = 1);

namespace Drupal\secret_test\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\TranslatableMarkup as TM;

/**
 * Provides a form for testing 'secret_select' element.
 */
final class SecretSelectForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'secret_test_test';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $test_settings['secrets'] = [
      'alpha' => 'secret',
      'beta' => 'secret',
      'gamma' => 'secret',
      'nested' => [
        'foo' => 'secret',
        'bar' => 'secret',
      ],
    ];
    new Settings(Settings::getAll() + $test_settings);

    $form['secret_1'] = [
      '#type' => 'secret_select',
      '#title' => new TM('Secret 1'),
      '#required' => TRUE,
    ];

    $form['secret_2'] = [
      '#type' => 'secret_select',
      '#title' => new TM('Secret 2'),
    ];

    $form['secret_3'] = [
      '#type' => 'secret_select',
      '#title' => new TM('Secret 3'),
      '#secret_path' => 'secrets/nested',
    ];

    $form['secret_4'] = [
      '#type' => 'secret_select',
      '#title' => new TM('Secret 4'),
      '#secret_path' => 'secrets/alpha',
    ];

    $form['secret_5'] = [
      '#type' => 'secret_select',
      '#title' => new TM('Secret 5'),
      '#secret_path' => 'not/existing/path',
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => new TM('Submit'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();
    $this->messenger()->addStatus(\json_encode($values));
  }

}
